import React, { useState } from 'react';
import './App.css';
import { suggestions } from './suggestions';

function App() {
  const [userInput, setUserInput] = useState('');
  const [filteredSuggestions, setFilteredSuggestions] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  let searchTimeout;

  const fetchSuggestions = async (query) => {
    console.log('query', query, suggestions);
    return new Promise((resolve, reject) => {

      searchTimeout = setTimeout(() => {

        const results = suggestions.filter(
          suggestion => suggestion.toLowerCase().includes(query.toLowerCase())
        );

        resolve(results);
      }, 1000);
    });

  };

  const onChangeHandler = async (e) => {
    setUserInput(e.target.value);
    console.log(e.target.value)

    if (searchTimeout) {
      searchTimeout.clear();
    }

    if (e.target.value === '') {
      console.log('suggestions', suggestions)
      setFilteredSuggestions([]);
      return;
    }


    setIsLoading(true);

    //fetching suggestions for userInput
    try {
      const results = await fetchSuggestions(e.target.value);
      setIsLoading(false)
      setFilteredSuggestions(results);
      console.log('filteredSuggestions', results);
    }
    catch (e) {
      setIsLoading(false);
      setFilteredSuggestions([]);
    }

  };

  const highlightText = (suggestion, userInput) => {
    const splitted = suggestion.toLowerCase().split(userInput.toLowerCase())
    let highlighted = <span><b>{userInput}</b></span>
    let temp = splitted.map((element) => <span>{element}</span>);
    let marker = [];
    for (let i in temp) {
      marker.push(temp[i]);

      //should not use == because i is string not number
      if (i != temp.length - 1) {
        marker.push(highlighted);

      }
    }
    return marker;
  };

  return (
    <div className="container">
      <h1>React Autocomplete test Deel</h1>
      <h2>Please start typing and check the autocomplete feature</h2>
      <input
        type="text"
        value={userInput}
        onChange={(e) => { onChangeHandler(e) }}
      />
      {!isLoading &&
        <ul>
          {filteredSuggestions &&
            filteredSuggestions.map((suggestion, index) => {
              const marker = highlightText(suggestion, userInput)
              return <li key={index}>{marker}</li>
            })
          }
        </ul>
      }
      {
        isLoading && <p>Loading...</p>
      }
    </div>
  );
}

export default App;
